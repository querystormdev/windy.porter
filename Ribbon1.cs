using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using QueryStorm.Tools.Excel;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Porter
{
    public class Ribbon1 : RibbonBase
    {
        IWorkbookAccessor workbookAccessor;
        private readonly ImportExportService service;

        public Ribbon1(IWorkbookAccessor workbookAccessor, ImportExportService importExportService)
    	{
            this.workbookAccessor = workbookAccessor;
            this.service = importExportService;
        }
        
       // For help on setting up the Ribbon XML, visit https://docs.microsoft.com/en-us/visualstudio/vsto/ribbon-overview?view=vs-2019
        public override string GetRibbonUI()
        {
            return 
@"<customUI xmlns='http://schemas.microsoft.com/office/2006/01/customui'
		xmlns:mso='http://schemas.microsoft.com/office/2006/01/customui'> 
   <ribbon>
     <tabs>
       <tab idQ='mso:TabData'> 
         <group id='group1' label='Porter'>
           <button id='btnExport' label='Export data' size='large' onAction='btnExport_Click' imageMso='ExportSharePointList'/> 
           <button id='btnImport' label='Import data' size='large' onAction='btnImport_Click' imageMso='ImportSharePointList'/> 
         </group>
       </tab>
     </tabs>
   </ribbon>
 </customUI>";
        }
        
        // example button click handler
        public void btnExport_Click(string controlId)
        {
        	service.Export();
    	}
        	
    	public void btnImport_Click(string controlId)
        	=> service.Import();
    }
}