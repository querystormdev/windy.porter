using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using QueryStorm.Apps;
using QueryStorm.Tools;
using Thingie.WPF.Controls.ObjectExplorer;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Porter
{
	public class ChooseObjectsDialog : Window
    {
        private readonly IEnumerable<string> tables;
        private readonly IEnumerable<string> variables;

        IEnumerable<NodeVM> tableNodes;
        IEnumerable<NodeVM> variableNodes;

        public HashSet<string> SelectedTables => tableNodes.Where(t => t.IsChecked == true).Select(n => n.Name).ToHashSet(StringComparer.OrdinalIgnoreCase);
        public HashSet<string> SelectedVariables => variableNodes.Where(t => t.IsChecked == true).Select(n => n.Name).ToHashSet(StringComparer.OrdinalIgnoreCase);

        public ChooseObjectsDialog(IEnumerable<string> tables, IEnumerable<string> variables)
        {
            this.Width = 700;
            this.Height = 500;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            this.tables = tables;
            this.variables = variables;

            var ouc = new Thingie.WPF.Controls.ObjectExplorer.ObjectExplorerUC();


            tableNodes = tables.Select(t => new ItemNodeVM(t)).ToList();
            variableNodes = variables.Select(v => new ItemNodeVM(v)).ToList();

            var tablesNode = new FolderNode("Tables");
            tableNodes.ForEach(tablesNode.Nodes.Add);

            var variablesNode = new FolderNode("Variables");
            variableNodes.ForEach(variablesNode.Nodes.Add);

            ouc.Nodes = new[] { variablesNode, tablesNode };

            var dockPanel = new DockPanel() { LastChildFill = true, Margin = new Thickness(10) };
            // var title = new TextBlock() { Text = "Select elements" };
            // title.FontSize = 24;
            // title.SetValue(DockPanel.DockProperty, Dock.Top);
            // dockPanel.Children.Add(title);
            var button = new Button();
            button.SetValue(DockPanel.DockProperty, Dock.Bottom);
            button.HorizontalAlignment = HorizontalAlignment.Right;
            button.Margin = new Thickness(5);
            button.Padding = new Thickness(10, 5, 10, 5);
            button.Content = "Ok";
            button.Click += (s, e) => { this.DialogResult = true; Close(); };
            dockPanel.Children.Add(button);
            dockPanel.Children.Add(ouc);

            Content = dockPanel;
        }


        class FolderNode : NodeVM
        {
            public FolderNode(string name) : base(name)
            {
                this.IsExpanded = true;
                this.IsChecked = true;
            }

            public override object Image => default;

            public override bool CheckboxEnabled => true;

            public override void Initialize()
            {
                base.Initialize();

                this.Nodes.ForEach(n => n.PropertyChanged += ChildPropChanged);
            }

            public override bool? IsChecked 
            { 
            	get => base.IsChecked; 
            	set 
            	{
        			base.IsChecked = value;
            		if(!reacting)
            		{
            			this.Nodes.ForEach(n => n.IsChecked = value);
            		}
            	}
        	}

            bool reacting;
            private void ChildPropChanged(object sender, PropertyChangedEventArgs args)
            {
                if (!reacting)
                {
                    reacting = true;
                    if (args.PropertyName == nameof(NodeVM.IsChecked))
                    {
                        if (this.Nodes.All(n => n.IsChecked == true))
                            this.IsChecked = true;
                        else if (this.Nodes.All(n => n.IsChecked == false))
                            this.IsChecked = false;
                        else
                            this.IsChecked = null;
                    }
                    reacting = false;
                }
            }
        }

        class ItemNodeVM : NodeVM
        {
            public ItemNodeVM(string name) : base(name)
            {
                this.IsChecked = true;
            }

            public override object Image => default;

            public override bool CheckboxEnabled => true;
        }
    }
}
