using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using QueryStorm.Engines;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Porter
{
	public class ImportExportService
    {
        private const string variablesTableName = "__variables";

        private readonly SQLiteEngineBuilder builder;
        private readonly IDialogService dialogService;
        private readonly IWorkbookAccessor workbookAccessor;
        private readonly ISyncRunner syncRunner;

        public ImportExportService(SQLiteEngineBuilder builder, IDialogService dialogService, IWorkbookAccessor workbookAccessor, ISyncRunner syncRunner)
        {
            this.builder = builder;
            this.dialogService = dialogService;
            this.workbookAccessor = workbookAccessor;
            this.syncRunner = syncRunner;
        }
        
        private IDataContext CreateDataContext()
        {
        	var dc = new WorkbookDataContext(workbookAccessor, syncRunner);
        	dc.Initialize();
        	return dc;
        }

        public void Export()
        {
        	var dataContext = CreateDataContext();
            var variables = GetVariables(dataContext);
            var dialog = new ChooseObjectsDialog(dataContext.Tables.Select(t => t.Name), variables.Select(v => v.Name));
            dialog.Title = "Select objects to export";
            if (dialog.ShowDialog() == true)
            {
                var selectedVariables = variables.Where(v => dialog.SelectedVariables.Contains(v.Name));
                var selectedTables = dataContext.Tables.Where(t => dialog.SelectedTables.Contains(t.Name));

                var varsTable = TabularBuilder.Instance.FromIEnumerable(selectedVariables.Select(v => new { v.Name, v.Value }), variablesTableName);

                var file = dialogService.DisplaySaveFileDialog("", "SQLite database file|*.sqlite");

                if (file == null)
                    return;

                if (File.Exists(file))
                    File.Delete(file);
                var engine = GetEngine(file);
                try
                {
                    foreach (var tbl in selectedTables.Union(new[] { varsTable }))
                    {
                        engine.AddTable(tbl);
                        engine.Execute($"create table [{tbl.Name}] as select * from [{tbl.Name}]");
                    }
                }
                finally
                {
                    engine.Disconnect();
                }
            }
        }

        public void Import()
        {
        	var dataContext = CreateDataContext();
            var file = dialogService.DisplayOpenFileDialog("SQLite database file|*.sqlite");
            if (file == null)
                return;

            var engine = GetEngine(file);
            try
            {
                var tableNames = engine.Execute($"select name from sqlite_master where type='table'").Select(r => r.Get<string>("name")).ToHashSet();
                var variableNames = new HashSet<string>();
                if(tableNames.Contains(variablesTableName))
                	variableNames  = engine.Execute($"select name from {variablesTableName}").Select(r => r.Get<string>("Name")).ToHashSet();

                var dialog = new ChooseObjectsDialog(tableNames.Except(new[] { variablesTableName }), variableNames);
                dialog.Title = "Select objects to import";
                if (dialog.ShowDialog() == true)
                {
                    foreach (var tbl in dataContext.Tables.Where(t => dialog.SelectedTables.Contains(t.Name)))
                    {
                        var data = engine.Execute($"select * from {tbl.Name}");
                        tbl.UpdateFrom(data);
                        tbl.SaveChanges();
                    }

                    if (variableNames.Count > 0)
                    {
                        var varsData = engine.Execute($"select * from {variablesTableName}");
                        foreach (var v in GetVariables(dataContext).Where(t => dialog.SelectedVariables.Contains(t.Name)))
                        {
                            var r = varsData.Find(v.Name, "Name");
                            v.Value = r.Get("Value");
                            v.Save();
                        }
                    }
                }
            }
            finally
            {
                engine.Disconnect();
            }
        }

        private IEnumerable<IVariable> GetVariables(IDataContext dataContext)
        {
            return dataContext.Variables
                .OfType<WorkbookVariable>()
                .Where(v => v.NameObj.RefersToRange.HasFormula is bool b && b == false)
                .ToList();
        }

        private IEngine GetEngine(string file)
        {
            var csBuilder = new SQLiteConnectionStringBuilder();
            csBuilder.DataSource = file;
            csBuilder.DateTimeFormat = SQLiteDateFormats.ISO8601;

            return builder
                .WithConnectionString(csBuilder.ConnectionString)
                .WithIncludeHiddenColumns(false)
                .Build();
        }
    }
}
